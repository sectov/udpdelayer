UDP Delayer
===========

Delays an UDP stream my an amount of milliseconds before passing it on.

Usage
-----

Specify the source (bind) address, the destination address and the amount of milliseconds to delay.

    ./udpdelayer <bind address> <dest address> <delay ms>

For example:

    ./udpdelayer 127.0.0.1:41234 127.0.0.1:51234 20000

License
-------

This software is released under the MIT license, see the LICENSE file.
