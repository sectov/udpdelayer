// UDP Delayer
// Copyright (c) Sectov 2019
// Distributed under the MIT License, see the LICENSE file.

use std::env;
use std::net::{SocketAddr, UdpSocket};
use std::time::{Duration, Instant};
use std::thread;
use std::sync::mpsc::channel;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main() {
    println!("UDPDelay v{}", VERSION);

    let (tx, rx) = channel();

    let args: Vec<String> = env::args().collect();

    if args.len() != 4 {
        println!("Usage: {} <bind address> <dest address> <delay ms>", args[0]);
        println!("   Ex: {} 127.0.0.1:41234 127.0.0.1:51234 20000", args[0]);
        return;
    }

    let delay: u64 = args[3].parse().expect("Delay should be a number");
    let delay = Duration::from_millis(delay);

    let dest: SocketAddr = args[2].parse().expect("Couldn't parse destination");

    let socket = UdpSocket::bind(&args[1]).expect("Couldn't bind to address");
    let send_socket = socket.try_clone().expect("Couldn't create sending socket");

    thread::spawn(move || {
        let mut buf = [0; 2048];
        loop {
            let (number_of_bytes, _) = socket.recv_from(&mut buf).expect("Couldn't receive from socket");
            let target = Instant::now() + delay;
            let filled_buf = &mut buf[..number_of_bytes];
            tx.send((target, filled_buf.to_owned())).expect("Error sending to channel");
        }
    });

    for (target, data) in rx.iter() {
        let now = Instant::now();
        if target > now {
            thread::sleep(target - now);
        }

        send_socket.send_to(&data, &dest).expect("Couldn't send the data");
    }

    println!("Receive channel closed, terminating");

}
